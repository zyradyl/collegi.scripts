## Collegi Script Repository ##

This repository contains all the scripts that are used in managing the Collegi
infrastructure. This should be noted to be a newer version of the former
`collegi-backup-script` repository, and as such will be more focused on any
scripts that are written for Collegi. This README file will expand more over
time, but is sufficient for right now. For licensing information, please
refer to `UNLICENSE` or `LICENSE`. Both files contain the same content.
